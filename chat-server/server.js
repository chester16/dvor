var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var mysql = require('mysql');
var squel = require('squel');

const uuidv1 = require('uuid/v1');
var users = [];

function uid() {
    return uuidv1();
}

var connDB = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'dvor',
    password: 'dvor',
    database: 'dvor'
});
connDB.connect(function (err) {
    if(err) throw err;
    console.log('connect');
});

function getMessages(callback) {
    let sql = squel.select()
        .from('messages')
        .toString();
    connDB.query(sql,function (error,results,field) {
       console.log(results);
       callback(results);
    });
}

function insertMessage(username,message) {
    let query = squel.insert()
        .into('messages')
        .set('username',username)
        .set('message',message)
        .toString();
    connDB.query(query,function (error, results, fields) {
        if (error) throw error;
        console.log(results);
    })
}

function sendAll(title , data){
    for(var i = 0; i< users.length ; i++){
        users[i]['socket'].emit(title,data);
    }
}


class User{
    constructor(socket,token){
        this.socket=socket;
        this.token=token;
    }
    setNickname(nickname){
        this.nickname = nickname;
    }
}

io.on('connection', function(socket){
    var token = uid();
    var newUser = new User(socket,token);
    users.push(newUser);
    getMessages(function (res) {
        socket.emit('token',{token:token});
        socket.emit('messages',{messages:res});
    });
    socket.on('chat_message',function (data) {
        console.log(data);
        //Запись в бд
        insertMessage(data['username'],data['message']);
        sendAll('chat_message',data);
    });

    socket.on('token',function (data) {
        socket.emit('token',{token:uid()});
    });
    socket.on('nickname',function (data) {
        var token = data['from'];
        for(var i in users){
            if(token == users[i]['token']){
                users[i].setNickname(data['nickname']);
            }
        }
    });
    socket.on('disconnect',function () {
        //users.splice(socket,1);
        console.log('disconnected');
    });

});
server.listen(7070,'localhost',function () {
    console.log('listen');
});