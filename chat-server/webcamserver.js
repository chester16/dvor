var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

io.on('connection', function(socket){

    socket.on('frame',function (data) {
        //console.log('frame ',JSON.stringify(data));
        socket.emit('frame',data['frame']);
    });

    socket.on('disconnect',function () {
        //users.splice(socket,1);
        console.log('disconnected');
    });

});
server.listen(6060,'localhost',function () {
    console.log('listen');
});