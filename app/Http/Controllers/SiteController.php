<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    public function index(){
        return view("start");
    }

    public function main(){
        return view('main');
    }

    public function chat(){
        return view('chat');
    }
    public function auth(){
        return "hello!";
    }
    public function webcamchat(){
        return view('webcamchat');
    }
}
