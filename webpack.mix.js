let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.disableNotifications();
mix.webpackConfig({
    module: {
        rules: [{
            test: /\.js?$/,
            use: [{
                loader: 'babel-loader',
                /*options: mix.config.babel()*/
                options: mix.config.babel()
            }]
        }]
    }
});

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.sass', 'public/css')
    .copyDirectory('resources/assets/images','public/images')
    .combine('resources/assets/js/es6/main/*.js', 'public/js/pages/main.js')
    .combine('resources/assets/js/es6/chat/*.js', 'public/js/pages/chat.js')
    .combine('resources/assets/js/es6/webcamchat/*.js', 'public/js/pages/webcamchat.js')
    .babel('public/js/pages/main.js','public/js/pages/main.js')
    .babel('public/js/pages/chat.js','public/js/pages/chat.js')
    .babel('public/js/pages/webcamchat.js','public/js/pages/webcamchat.js')
    /*.babel('resources/assets/js/es6/main/!*.js','public/js/pages/main.js')
    .babel('resources/assets/js/es6/chat/!*.js','public/js/pages/chat.js')
    .babel('resources/assets/js/es6/webcamchat/!*.js','public/js/pages/webcamchat.js')*/
