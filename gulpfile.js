var gulp = require('gulp');
var sass = require('gulp-sass');
var babel = require('gulp-babel');

var paths = {
    scss : ["resources/assets/sass/app.scss","public/css","resources/assets/sass/**/*.scss"],
    js: ["resources/assets/js/es6/**/*.js","public/js"]
}

gulp.task('sass', function () {
  return gulp.src(paths.scss[0])
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(paths.scss[1]));
});

gulp.task('babel',function(){
    return gulp.src(paths.js[0])
        .pipe(babel())
        .pipe(babel({
            "presets": ["es2015"],
            "plugins": ["remove-comments"]
        }))
        .pipe(gulp.dest(paths.js[1]));
});

gulp.task('watch', function () {
  gulp.watch(paths.scss[2], ['sass']);
  gulp.watch(paths.js[0],['babel']);
});