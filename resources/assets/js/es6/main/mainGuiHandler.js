class MainGuiHandler{
    constructor(){
        let self = this;
        self.loginTab = $('.login-tab');
        self.regTab = $('.register-tab');

        self.init();
    }

    init(){
        let self = this;
        $(self.loginTab).click(function(e){
            $('.register-tab').removeClass('active');
            $('.register-tab').addClass('no-active');
            $('.login-tab').removeClass('no-active');
            $('.login-tab').addClass('active');

            $('.reg-form-block').hide(250,function(){
                $('.login-form-block').show(250);
            });

        });
        $(self.regTab).click(function(e){
            $('.login-tab').removeClass('active');
            $('.login-tab').addClass('no-active');
            $('.register-tab').removeClass('no-active');
            $('.register-tab').addClass('active');

            $('.login-form-block').hide(250,function(){
                $('.reg-form-block').show(250);
            });
        });

        $('.login-form-block .common-form').submit(function (e) {
            e.preventDefault();
            var form = this;
            let email = $(this).find('input#login').val();
            let password = $(this).find('input#password').val();
            $.ajax({
               url: '/login',
               type:'post',
               data:{
                   _token: $('meta[name=csrf-token]').attr('content'),
                   email:email,
                   password:password
               },
               success:function (data) {
                   console.log('gg');
               },
               complete:function(data){
                   let status = data.status;
                   if(status == 422){
                       console.log('error');
                       $(form).find('input.form-input').addClass('error');
                   }
                   else if(status == 200)
                   {
                       document.location.href = "/main";
                   }
                    console.log('comlete ',data.status);
               },

           })
        });


        $('.reg-form-block .common-form').submit(function (e) {
            e.preventDefault();
            var form = this;
            let name =$(this).find('input#name').val();
            let email = $(this).find('input#email').val();
            let nickname = $(this).find('input#username').val();
            let password = $(this).find('input#password').val();
            let password_confirmation = $(this).find('input#password_confirmation').val();
            $.ajax({
                url: '/register',
                type:'post',
                data:{
                    _token: $('meta[name=csrf-token]').attr('content'),
                    name: name,
                    username: nickname,
                    email:email,
                    password:password,
                    password_confirmation:password_confirmation
                },
                success:function (data) {
                    console.log('gg');
                },
                complete:function(data){
                    let status = data.status;
                    if(status == 422){
                        console.log('error');
                        $(form).find('input.form-input').addClass('error');
                    }
                    else if(status == 200)
                    {
                        document.location.href = "/main";
                    }
                    console.log('comlete ',data.status);
                },

            })
        });

        $('.logout').click(function (e) {
            e.preventDefault();
           $('#logout-form').submit();

        });
    }
}