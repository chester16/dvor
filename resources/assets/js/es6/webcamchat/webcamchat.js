var video = $("#cam")[0];
var socket = io('127.0.0.1:6060');
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

if (navigator.getUserMedia) {
    navigator.getUserMedia({video: true}, handleVideo, videoError);
}

function handleVideo(stream) {
    //console.log('video ,',video);
    video.src = window.URL.createObjectURL(stream);
    video.play();
}

function videoError(e) {
    // do something
}

video.addEventListener('play',function (e) {
   let ctx = $('#canvas')[0].getContext('2d');
   //ctx.drawImage(this,0,0,200,200);
    draw(this,ctx);
});

function getBase64Image(img) {
    // Create an empty canvas element
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    // Get the data-URL formatted image
    // Firefox supports PNG and JPEG. You could check img.src to guess the
    // original format, but be aware the using "image/jpg" will re-encode the image
    var dataURL = canvas.toDataURL("image/png");
    return dataURL;
}

function draw(video,ctx) {
    ctx.drawImage(video,0,0,200,200);
    let frame = ctx.getImageData(0,0,200,200);
    let data = $('#canvas')[0].toDataURL();
    //console.log(data);
    socket.emit('frame',{frame:data});

    setTimeout(draw,1000/30,video,ctx);
}


socket.on('frame',function(data){
    //console.log(data);

    var img = new Image;
    img.src = data;
    let ctx = $('#canvas3')[0].getContext('2d');
    ctx.drawImage(img,0,0);
    /*let uarray = new Uint8ClampedArray(data['frame']);
    console.log('my framee ', uarray);
    let iData = new ImageData(data['frame'],200,200);
    //iData.data = new Uint8ClampedArray(data['frame']);
    let ctx2 = $('#canvas2')[0].getContext('2d');
    ctx2.putImageData(iData,0,0);*/
});
