$(document).ready(function () {
    var wsConnection = new ChatConnection();
    var guiHandler ;
    console.log('nickname ',$('.chat-block .chat-info').attr('username'));
    let username = $('.chat-block .chat-info').attr('username');
    if(username != ''){
        wsConnection.init('127.0.0.1:7070',username);
        guiHandler = new GuiHandler(wsConnection);
        guiHandler.init();
    }
    $('.chat-login').submit(function (e) {
        e.preventDefault();
        $('.chat-block .login-block').hide();
        wsConnection.init('127.0.0.1:7070',$(this).find('#nickname').val());
        guiHandler = new GuiHandler(wsConnection);
        guiHandler.init();
    });


});