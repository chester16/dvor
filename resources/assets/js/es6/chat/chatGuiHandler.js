class GuiHandler{
    constructor(wsConnection){
        var self = this;
        self.wsConnection = wsConnection;
        self.inputBlock = $('.input-block');
        self.textBlock = $('.text-block');

        self.chatVUE = new Vue({
            el:'#app-chat',
            data:{
                messages:[
                    //{username:'test',text:'hello chat!'},
                ],
                methods:{
                    sendMessage(){
                        let text =$('.input-block').val();
                        self.sendMessage(text);
                        $('.input-block').val('');
                    },
                }
            }
        })
    }
    init(){
        //инициализация эвентов и приема данных с сокета
        let self = this;
        $(self.inputBlock).keypress(function(e){
            console.log(123);
            if(e.which == 13){
                //тут отправка сообщения по сокету на сервер
                let text = $(self.inputBlock).val();
                self.sendMessage(text);
                $(self.inputBlock).val('');
            }
        });


        self.wsConnection.socket.on('chat_message',function (data) {
            console.log(data);
            //$(self.textBlock).append("<p>",data.username,": ",data.message,"</p>");
            self.chatVUE.messages.push({username:data.username,text:data.message});
        });
        self.wsConnection.socket.on('messages',function (data) {
            for (var i in data.messages){
                //console.log(data.messages[i]);
                //$(self.textBlock).append("<p>",data.messages[i].username,": ",data.messages[i].message,"</p>");
                self.chatVUE.messages.push({username:data.messages[i].username,text:data.messages[i].message});
            }
        });
    }

    sendMessage(message){
        let self = this;
        console.log('fff',self.wsConnection.username);
        this.wsConnection.send('chat_message',{message:message,username:self.wsConnection.nickname});
        $(self.inputBlock).val('');
    }
}