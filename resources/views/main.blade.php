@extends('layouts/dvor')

@section('body')
    <script src="{{asset("js/pages/main.js")}}"></script>
    <div class="main-body-block">
        <div class="row flexbox">
            <div class="flex-item">Крокодил</div>
            <div class="flex-item"><a href="{{url('/webcamchat')}}">Видео чат</a></div>
        </div>
        <div class="row flexbox">
            <div class="flex-item"><a href="{{url("/chat")}}">Текстовый чат, румы</a></div>
            <div class="flex-item">
                <div class="profile-block">
                    @if(\Illuminate\Support\Facades\Auth::check())
                        Профиль {{\Illuminate\Support\Facades\Auth::user()->username}} <a class="logout" href="">Quit</a>
                        <div class="header-block">
                            <div class="avatar-block"><img  alt="Аватар" width="300" height="300"></div>
                            <div class="user-info-block">
                                <div class="charachter-block">USERNAME: DartVader</div>
                                <div class="charachter-block">Age: 999</div>
                                <div class="charachter-block">City: StarOfDeath</div>
                            </div>
                        </div>
                        <div class="achievments-block">Achiev</div>
                        <div class="description-block">Описание: черный вояка в маске с мечом</div>
                    @else
                        <div class="tab-block">
                            <div class="header">
                                <div class="item login-tab active" id="0">Войти</div>
                                <div class="item register-tab no-active" id="1">Регистрация</div>
                            </div>
                            <div class="body">
                                <div class="login-form-block" >
                                    <form action="#" class="common-form">
                                        <div class="form-group">
                                            <label for="login">Логин</label>
                                            <input id="login" type="email" class="form-input" name="email" placeholder="email..">
                                        </div>
                                        <div class="form-group">
                                            <label for="login">Пароль</label>
                                            <input id="password" class="form-input" type="password" name="password">
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="common-btn" value="Войти">
                                        </div>
                                    </form>
                                </div>
                                <div class="reg-form-block">
                                    <form action="#" class="common-form">
                                        <div class="form-group">
                                            <label for="login">Логин</label>
                                            <input id="name" class="form-input" type="text" name="name">
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Никнейм</label>
                                            <input id="username" class="form-input" type="text" name="username">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Почта</label>
                                            <input id="email" class="form-input" type="email" name="email">
                                        </div>
                                        <div class="form-group">
                                            <label for="login">Пароль</label>
                                            <input id="password" class="form-input" type="password" name="password">
                                        </div>
                                        <div class="form-group">
                                            <label for="login">Пароль repeat</label>
                                            <input id="password_confirmation" class="form-input" type="password" name="password_confirmation">
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="common-btn" value="Регистрация">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    @endif
                </div>

            </div>
        </div>
    </div>
@endsection
