@extends('layouts/dvor')

@section('body')
    <div class="webcamblock">
        <video width="200" height="200" autoplay="true" id="cam"></video>
        <canvas width="200" height="200" id="canvas"></canvas>
        <canvas width="200" height="200" id="canvas2"></canvas>
        <canvas width="200" height="200" id="canvas3"></canvas>
    </div>
    <script src="{{asset("js/pages/webcamchat.js")}}"></script>
@endsection