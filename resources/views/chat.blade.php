@extends('layouts/dvor')
@section('head') @endsection
@section('body')
    <script src="{{asset("js/pages/chat.js")}}"></script>
    <div class="chat-block" id="app-chat">
        <div class="chat-info" style="display: none;" username="@if(\Illuminate\Support\Facades\Auth::check()){{\Illuminate\Support\Facades\Auth::user()->username}}@endif"></div>
        <div class="row flexbox">
            @if(!\Illuminate\Support\Facades\Auth::check())
                <div class="flex-item login-block" style="display: block;">
                    <form class="chat-login" action="#">
                        <input class="text" type="text" placeholder="Никнейм" id="nickname">
                        <input class="submit" type="submit" value="Войти">
                    </form>
                </div>
            @endif
            <div class="flex-item work-block">
                <div class="messages-block">
                    <div class="text-block">
                        <div class="chat-message" v-for="msg in messages">
                            @{{msg.username}}: @{{ msg.text }}
                        </div>

                    </div>
                    <input type="text" class="input-block" v-on:keyup.13="methods.sendMessage">

                </div>
                <div class="users-block">list</div>
            </div>

        </div>
    </div>
@endsection
@section('footer') @endsection