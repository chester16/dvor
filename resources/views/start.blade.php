@extends('layouts/dvor')

@section('body')
    <div class="start-wrapper" id="start-app">
        <transition name="earth-fade" v-on:after-leave="earthAnimAfterLeave">
            <div class="earth-block earth-spin" v-if="earth_anim">
                <img src="{{asset('images/earth.svg')}}" alt="">
            </div>
        </transition>
        <transition name="hw-jump">
            <div class="hello-world-block" @click="hello" v-if="hw_anim">
                {{--<a href="{{url('/main')}}"><img src="{{asset('images/helloworld.svg')}}" alt="">!</a>--}}
                <a href="#"><img src="{{asset('images/helloworld.svg')}}" alt="">!</a>
            </div>
        </transition>
    </div>

    <script>
        new Vue({
            el: "#start-app",
            data: function () {
                return {
                    earth_anim : true,
                    hw_anim : true,
                }
            },
            methods: {
                hello() {
                    this.earth_anim = !this.earth_anim;
                    this.hw_anim = !this.hw_anim;
                },
                earthAnimAfterLeave(){
                    window.location.href = "/main";
                }
            }
        });
    </script>
@endsection
