@extends('layouts.app')

@section('content')
    @if(\Illuminate\Support\Facades\Auth::check())
        <p>{{\Illuminate\Support\Facades\Auth::user()->email}}</p>
    @endif
@endsection