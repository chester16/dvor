var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

$(document).ready(function () {
    var mainGuiHandler = new MainGuiHandler();
});

var MainGuiHandler = function () {
    function MainGuiHandler() {
        _classCallCheck(this, MainGuiHandler);

        var self = this;
        self.loginTab = $('.login-tab');
        self.regTab = $('.register-tab');

        self.init();
    }

    _createClass(MainGuiHandler, [{
        key: 'init',
        value: function init() {
            var self = this;
            $(self.loginTab).click(function (e) {
                $('.register-tab').removeClass('active');
                $('.register-tab').addClass('no-active');
                $('.login-tab').removeClass('no-active');
                $('.login-tab').addClass('active');

                $('.reg-form-block').hide(250, function () {
                    $('.login-form-block').show(250);
                });
            });
            $(self.regTab).click(function (e) {
                $('.login-tab').removeClass('active');
                $('.login-tab').addClass('no-active');
                $('.register-tab').removeClass('no-active');
                $('.register-tab').addClass('active');

                $('.login-form-block').hide(250, function () {
                    $('.reg-form-block').show(250);
                });
            });

            $('.login-form-block .common-form').submit(function (e) {
                e.preventDefault();
                var form = this;
                var email = $(this).find('input#login').val();
                var password = $(this).find('input#password').val();
                $.ajax({
                    url: '/login',
                    type: 'post',
                    data: {
                        _token: $('meta[name=csrf-token]').attr('content'),
                        email: email,
                        password: password
                    },
                    success: function success(data) {},
                    complete: function complete(data) {
                        var status = data.status;
                        if (status == 422) {
                            $(form).find('input.form-input').addClass('error');
                        } else if (status == 200) {
                            document.location.href = "/main";
                        }
                    }

                });
            });

            $('.reg-form-block .common-form').submit(function (e) {
                e.preventDefault();
                var form = this;
                var name = $(this).find('input#name').val();
                var email = $(this).find('input#email').val();
                var nickname = $(this).find('input#username').val();
                var password = $(this).find('input#password').val();
                var password_confirmation = $(this).find('input#password_confirmation').val();
                $.ajax({
                    url: '/register',
                    type: 'post',
                    data: {
                        _token: $('meta[name=csrf-token]').attr('content'),
                        name: name,
                        username: nickname,
                        email: email,
                        password: password,
                        password_confirmation: password_confirmation
                    },
                    success: function success(data) {},
                    complete: function complete(data) {
                        var status = data.status;
                        if (status == 422) {
                            $(form).find('input.form-input').addClass('error');
                        } else if (status == 200) {
                            document.location.href = "/main";
                        }
                    }

                });
            });

            $('.logout').click(function (e) {
                e.preventDefault();
                $('#logout-form').submit();
            });
        }
    }]);

    return MainGuiHandler;
}();