var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

$(document).ready(function () {
    var wsConnection = new ChatConnection();
    var guiHandler;

    var username = $('.chat-block .chat-info').attr('username');
    if (username != '') {
        wsConnection.init('127.0.0.1:7070', username);
        guiHandler = new GuiHandler(wsConnection);
        guiHandler.init();
    }
    $('.chat-login').submit(function (e) {
        e.preventDefault();
        $('.chat-block .login-block').hide();
        wsConnection.init('127.0.0.1:7070', $(this).find('#nickname').val());
        guiHandler = new GuiHandler(wsConnection);
        guiHandler.init();
    });
});

var ChatConnection = function () {
    function ChatConnection(ws) {
        _classCallCheck(this, ChatConnection);
    }

    _createClass(ChatConnection, [{
        key: 'init',
        value: function init(ws, nickname) {
            var self = this;
            this.nickname = nickname;
            this.socket = io(ws);

            this.socket.on('token', function (data) {
                self.setToken(data.token);
                self.send('nickname', { nickname: self.nickname });
            });
        }
    }, {
        key: 'refreshToken',
        value: function refreshToken() {
            this.socket.emit('token', { from: this.nickname });
        }
    }, {
        key: 'setToken',
        value: function setToken(newToken) {
            this.token = newToken;
        }
    }, {
        key: 'getToken',
        value: function getToken() {
            return this.token;
        }
    }, {
        key: 'send',
        value: function send(title, data) {
            var finalData = data;
            finalData.from = this.token;
            this.socket.emit(title, data);
        }
    }]);

    return ChatConnection;
}();

var GuiHandler = function () {
    function GuiHandler(wsConnection) {
        _classCallCheck(this, GuiHandler);

        var self = this;
        self.wsConnection = wsConnection;
        self.inputBlock = $('.input-block');
        self.textBlock = $('.text-block');

        self.chatVUE = new Vue({
            el: '#app-chat',
            data: {
                messages: [],
                methods: {
                    sendMessage: function sendMessage() {
                        var text = $('.input-block').val();
                        self.sendMessage(text);
                        $('.input-block').val('');
                    }
                }
            }
        });
    }

    _createClass(GuiHandler, [{
        key: 'init',
        value: function init() {
            var self = this;
            $(self.inputBlock).keypress(function (e) {
                if (e.which == 13) {
                    var text = $(self.inputBlock).val();
                    self.sendMessage(text);
                    $(self.inputBlock).val('');
                }
            });

            self.wsConnection.socket.on('chat_message', function (data) {
                self.chatVUE.messages.push({ username: data.username, text: data.message });
            });
            self.wsConnection.socket.on('messages', function (data) {
                for (var i in data.messages) {
                    self.chatVUE.messages.push({ username: data.messages[i].username, text: data.messages[i].message });
                }
            });
        }
    }, {
        key: 'sendMessage',
        value: function sendMessage(message) {
            var self = this;

            this.wsConnection.send('chat_message', { message: message, username: self.wsConnection.nickname });
            $(self.inputBlock).val('');
        }
    }]);

    return GuiHandler;
}();