var video = $("#cam")[0];
var socket = io('127.0.0.1:6060');
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

if (navigator.getUserMedia) {
    navigator.getUserMedia({ video: true }, handleVideo, videoError);
}

function handleVideo(stream) {
    video.src = window.URL.createObjectURL(stream);
    video.play();
}

function videoError(e) {}

video.addEventListener('play', function (e) {
    var ctx = $('#canvas')[0].getContext('2d');

    draw(this, ctx);
});

function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    var dataURL = canvas.toDataURL("image/png");
    return dataURL;
}

function draw(video, ctx) {
    ctx.drawImage(video, 0, 0, 200, 200);
    var frame = ctx.getImageData(0, 0, 200, 200);
    var data = $('#canvas')[0].toDataURL();

    socket.emit('frame', { frame: data });

    setTimeout(draw, 1000 / 30, video, ctx);
}

socket.on('frame', function (data) {

    var img = new Image();
    img.src = data;
    var ctx = $('#canvas3')[0].getContext('2d');
    ctx.drawImage(img, 0, 0);
});