<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index');
Route::get('/main', 'SiteController@main');
Route::get('/chat', 'SiteController@chat');
Route::post('/auth','SiteController@auth');
Route::get('/webcamchat','SiteController@webcamchat');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


